# Instrucciones de instalación
- Tener la version de PHP 8.0.2 o superior.
- En el archivo de entorno .env se debe agregar el valor para las variables API_URL y API_TOKEN
- Ejecutar el comando: composer install
- Luego tendriamos que crear la base de datos y agregar el nombre de la misma, usuario y contraseña al archivo .env
- Una vez tengamos conexion existosa,  debemos ejecutar el comando: php artisan migrate y luego ejecutar el comando: php artisan db:seed (lo que generará el usuario con el que podras acceder a ver el desarrollo)
Usuario: admin@admin.com
Contraseña: password
- Luego debemos ejecutar el comando: npm install
- Luego debemos ejecutar npm run build (Para generar la compilacion del manifest.json y que no falle la apliacion al renderizar las vistas, SOLO si lo haras en ambiente productivo o si no deseas correr el modo desarrollo de npm)
- Luego ejecutar el comando: php artisan serve y se ejecutara el proyecto por el puerto 8000 de localhost
