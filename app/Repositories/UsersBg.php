<?php

namespace App\Repositories;

use GuzzleHttp\Client;

/**
 * Metodos de la clase UserBg
 * Realizados para hacer el procesamiento de los datos obtenido del api
 */
class UsersBg {
    protected $token;
    protected $client;

    /**
     * Constructor donde inyectamos las dependencias de GuzzleHttp\Client
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->token = env('API_TOKEN');
    }

    public function all(){
        return json_decode($this->get(),1);
    }

    public function find($id){
        $url = "/transaction/{$id}";
        return json_decode($this->get($url),1);
    }
    public function get($url ='')
    {
        $response = $this->client->request('GET', "users/{$this->token}{$url}");
        return $response->getBody()->getContents();
    }

}
