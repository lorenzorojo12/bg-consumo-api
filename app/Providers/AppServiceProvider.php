<?php

namespace App\Providers;

use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;
use Illuminate\Pagination\Paginator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Le indicamos al framwork que nos cree la instancia a la api
        $this->app->singleton('GuzzleHttp\Client', function (){
            return new Client([
                'base_uri' => env('API_URL')
            ]);
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Se realiza para que la paginacion por defecto sea de bootstrap5
        Paginator::useBootstrapFive();
    }
}
