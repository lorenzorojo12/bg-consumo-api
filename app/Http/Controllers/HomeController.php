<?php

namespace App\Http\Controllers;

use App\Repositories\UsersBg;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Http;
use GuzzleHttp\Client;
use App\Helpers;

class HomeController extends Controller
{
    protected $usersBg;
    /**
     * Create a new controller instance.
     * Realizamos la inyeccion de la dependencia
     * @return void
     */
    public function __construct(UsersBg $usersBg)
    {
        $this->usersBg = $usersBg;
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     * @return Renderable
     */
    public function index(Request $request)
    {
        // En este proceso creamos una paginacion customizada, ya que el api no devuelve paginacion alguna
        $extension = $request->extension;
        $perPage = 10;
        $currentPage = $request->page ?? 1;
        $offset = ($currentPage-1)*$perPage;
        $dataUsers = $this->usersBg->all();
        $count = count($dataUsers);
        // pasamos el array obtenido para ordenarlo por fecha DESC
        $items = Helpers::array_sort($dataUsers, 'created_at', SORT_DESC);
        // Dividimos el array
        $data = array_slice($items,$offset,$perPage,true);
        // Generamos la instancia del Paginador
        $data = new LengthAwarePaginator($data,$count,$perPage,$currentPage,[
            'path' => $request->url(),
            'query' => $request->query()
        ]);
        // Devolvemos la informacion a la vista.
        return view('home')
            ->with('data',$data);
    }

    public function show(Request $request, $id){
        // En este proceso creamos una paginacion customizada, ya que el api no devuelve paginacion alguna
        $extension = $request->extension;
        $perPage = 10;
        $currentPage = $request->page ?? 1;
        $offset = ($currentPage-1)*$perPage;
        $dataUsers = $this->usersBg->find($id);
        $count = count($dataUsers);
        // pasamos el array obtenido para ordenarlo por fecha DESC
        $items = Helpers::array_sort($dataUsers, 'created_at', SORT_DESC);
        // Dividimos el array
        $data = array_slice($items,$offset,$perPage,true);
        // Generamos la instancia del Paginador
        $data = new LengthAwarePaginator($data,$count,$perPage,$currentPage,[
            'path' => $request->url(),
            'query' => $request->query()
        ]);
        // Devolvemos la informacion a la vista.
        return view('transactions')
            ->with([
                'transactions' => $data,
                'userBgId' => $id
            ]);
    }

}
