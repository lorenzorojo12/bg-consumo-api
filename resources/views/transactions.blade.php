@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a href="{{route('home')}}" style="text-decoration: none;">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-left-short" viewBox="0 0 16 16">
                                <path fill-rule="evenodd" d="M12 8a.5.5 0 0 1-.5.5H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5a.5.5 0 0 1 .5.5z"></path>
                            </svg>
                        </a>
                        {{ __('Transacciones del usuario') }} <b>{{$userBgId}}</b>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if(count($transactions)>0)
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">id</th>
                                        <th scope="col">client_id</th>
                                        <th scope="col">segmentation_id</th>
                                        <th scope="col">transaction_type_id</th>
                                        <th scope="col">transaction_status_id</th>
                                        <th scope="col">transaction_currency_id</th>
                                        <th scope="col">transaction_source_id</th>
                                        <th scope="col">year</th>
                                        <th scope="col">month</th>
                                        <th scope="col">amount</th>
                                        <th scope="col">savings_expiration_date</th>
                                        <th scope="col">transaction_detail</th>
                                        <th scope="col">created_by</th>
                                        <th scope="col">updated_by</th>
                                        <th scope="col">created_at</th>
                                        <th scope="col">updated_at</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($transactions as $transaction)
                                        <tr>
                                            <th>{{$transaction["id"]}}</th>
                                            <th>{{$transaction["client_id"]}}</th>
                                            <th>{{$transaction["segmentation_id"]}}</th>
                                            <th>{{$transaction["transaction_type_id"]}}</th>
                                            <th>{{$transaction["transaction_status_id"]}}</th>
                                            <th>{{$transaction["transaction_currency_id"]}}</th>
                                            <th>{{$transaction["transaction_source_id"]}}</th>
                                            <th>{{$transaction["year"]}}</th>
                                            <th>{{$transaction["month"]}}</th>
                                            <th>{{$transaction["amount"]}}</th>
                                            <th>{{$transaction["savings_expiration_date"]}}</th>
                                            <th>{{$transaction["transaction_detail"]}}</th>
                                            <th>{{$transaction["created_by"]}}</th>
                                            <th>{{$transaction["updated_by"]}}</th>
                                            <th>{{$transaction["created_at"]}}</th>
                                            <th>{{$transaction["updated_at"]}}</th>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{$transactions->links()}}
                            </div>
                        @else
                            <h2 class="text-center">El usuario no tiene transacciones</h2>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
