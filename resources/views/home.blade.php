@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><h2 class="text-center">{{ __('Listado de Usuarios') }}</h2></div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if(count($data)>0)
                            <div class="table-responsive">
                                <table class="table display display dataTable" id="tableUsers">
                                    <thead>
                                    <tr>
                                        <th scope="col">id</th>
                                        {{--<th scope="col">segmentation_id</th>
                                        <th scope="col">program_id</th>--}}
                                        <th scope="col">user_id</th>
                                        <th scope="col">netcommerce_id</th>
                                        {{--<th scope="col">h2a_token</th>--}}
                                        {{--<th scope="col">one_signal_player_id</th>--}}
                                        {{--<th scope="col">firma</th>--}}
                                        {{--<th scope="col">identification_type_id</th>--}}
                                        {{--<th scope="col">identification_number</th>--}}
                                        {{-- <th scope="col">mobile_number</th>--}}
                                        {{--<th scope="col">meta</th>--}}
                                        <th scope="col">insitu_code_reference</th>
                                        <th scope="col">birth_date</th>
                                        <th scope="col">active</th>
                                        <th scope="col">has_updated_info</th>
                                        <th scope="col">inactivate_reason</th>
                                        <th scope="col">account_lockout_date</th>
                                        <th scope="col">state_user_id</th>
                                        <th scope="col">created_at</th>
                                        {{--<th scope="col">updated_at</th>--}}
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($data as $user)
                                        <tr>
                                            <td><a href="{{route('show',$user["id"])}}">{{$user["id"]}}</a></td>
                                            {{--<td>{{$user["segmentation_id"]}}</td>
                                            <td>{{$user["program_id"]}}</td>--}}
                                            <td>{{$user["user_id"]}}</td>
                                            <td>{{$user["netcommerce_id"]}}</td>
                                            {{--<td>{{$user["h2a_token"]}}</td>--}}
                                            {{--<td>{{$user["one_signal_player_id"]}}</td>--}}
                                            {{--<td>{{$user["firma"]}}</td>--}}
                                            {{--<td>{{$user["identification_type_id"]}}</td>--}}
                                            {{--<td>{{$user["identification_number"]}}</td>--}}
                                            {{--<td>{{$user["mobile_number"]}}</td>
                                            <td>{{$user["meta"]}}</td>--}}
                                            <td>{{$user["insitu_code_reference"]}}</td>
                                            <td>{{$user["birth_date"]}}</td>
                                            <td>{{$user["active"]}}</td>
                                            <td>{{$user["has_updated_info"]}}</td>
                                            <td>{{$user["inactivate_reason"]}}</td>
                                            <td>{{$user["account_lockout_date"]}}</td>
                                            <td>{{$user["state_user_id"]}}</td>
                                            <td>{{$user["created_at"]}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{$data->links()}}
                            </div>
                        @else
                            <h2 class="text-center">No existen datos</h2>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
@endsection
